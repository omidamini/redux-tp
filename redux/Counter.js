import {Image,TextInput, Dimensions, StyleSheet, View, Text, Pressable} from 'react-native'
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment, incrementByAmount,addIfOdd , incrementAsync } from './CounterSlice'

const Counter = () => {
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()
    const [text, setText] = useState('0');
  return (
    <View style={styles.container}>
           <View style={styles.col}>
            <View style={{flexDirection: 'row',justifyContent: 'center'}}>
              <Image source={require('../assets/logo.jpg')} style={styles.image}/>
            </View>
            <View style={{flexDirection:'row',}}>
                <View style={styles.col}>
                    <Pressable style={styles.button}  onPress={() => dispatch(decrement())}>
                      <Text>-</Text> 
                    </Pressable>
                </View>

                <View style={styles.col}>
                  <Text style={styles.text}>{count}</Text>
                </View>
                <View style={styles.col}>
                    <Pressable style={styles.button} onPress={() => dispatch(increment())}>
                      <Text>+</Text> 
                    </Pressable>
                </View>
            </View>
            <View style={{flexDirection: 'row',justifyContent: 'center'}}>
                <View style={styles.col}>
                      <TextInput keyboardType='numeric' style={styles.textInput} onChangeText={(text) => setText(text)} value = {text}></TextInput>
                </View>
                <View style={styles.col}>
                    <Pressable style={styles.button} onPress={() => dispatch(incrementByAmount(text))}>
                      <Text>Add amount</Text> 
                    </Pressable>
                </View>
                <View style={styles.col}>
                    <Pressable style={styles.button} onPress={() => dispatch(incrementAsync(text))}>
                      <Text>Add Async</Text> 
                    </Pressable>
                </View>
                <View style={styles.col}>
                    <Pressable style={styles.button} onPress={() => dispatch(addIfOdd(text))}>
                      <Text>Add if Odd</Text> 
                    </Pressable>
                </View>
            </View>
        </View>
    </View>
  )
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
export default Counter;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap:'nowrap',
    flexDirection:'row',
    width:deviceWidth,
    height : deviceHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },
    col:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
    },
    button : {
        backgroundColor : '#F0EEF8',
        padding: 5,
        marginLeft: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderColor:'#B8A7DB',
        borderWidth:1,
    },
    text: {
      margin: 20,
      fontSize: 70,

    },
    image: {
      justifyContent:'center',
      alignItems:'center',
      borderTopLeftRadius:10,
      borderTopRightRadius:10,
      height: 200,
      width: 200,
      backgroundColor:'red'
    },
    textInput : {
      borderColor:'#000',
      borderWidth:0.5,
      width:50,
      textAlign:'center'
    }


  });
  