
import { StatusBar } from 'expo-status-bar';
import {SafeAreaView, Dimensions, StyleSheet, ScrollView, Text, View } from 'react-native';
import store from './redux/store'
import Counter from './redux/Counter'
import {Provider as ReduxProvider} from 'react-redux';


const App = () =>  {
  const MainMarkup = () => {
    return (
    <SafeAreaView style={{height:'100%'}}>
      <StatusBar />
      <ScrollView style={styles.scrollView}>
        <View style={styles.ItemContainer}>
          <Counter />
        </View>
      </ScrollView>
    </SafeAreaView>)
  };
  return (
    <ReduxProvider store={store}>
      <MainMarkup />
    </ReduxProvider>
  );
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:deviceWidth,
    height : deviceHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollView: {
    flexDirection:'column',
},
ItemContainer: {
  flexWrap: 'wrap',
  flex:1,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems:'center',
  marginTop: 20,
  paddingTop: 0,
  marginBottom: 20,
  paddingBottom: 20,
  borderRadius: 10,
},
});


